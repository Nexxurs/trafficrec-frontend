Run this project in Android Studio. The gradle project should be automatically loaded and the app
can be installed on any connected device via Android Studio.

## Emulator
If you want to use this project in an emulator, please choose a device with "Play Store" installed 
and choose an API Level of 32 or higher.