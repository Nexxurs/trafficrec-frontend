package at.tuwien.e1448422.trafficrec.utils

interface SuccessFailureCallback {
    fun onSuccess()
    fun onFailure(errorMessage: String)
}