package at.tuwien.e1448422.trafficrec.camera_processing

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.Log
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.preference.PreferenceManager
import at.tuwien.e1448422.trafficrec.R
import at.tuwien.e1448422.trafficrec.utils.imageconversion.BitmapUtils
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.ByteArrayOutputStream
import java.lang.Exception
import java.net.ProtocolException
import java.util.function.Consumer


class SignAnalyzer(context: Context, val onSignCallback: Consumer<String>, val onExceptionCallback: Consumer<Exception>) : ImageAnalysis.Analyzer {
    private val TAG = "SignAnalyzer"
    private val client = OkHttpClient()
    private val connectionURL: String

    var targetRotation: Int = 0

    init {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        connectionURL = sharedPreferences.getString(context.getString(R.string.connection_url), "")!!
    }

    @SuppressLint("UnsafeOptInUsageError")
    override fun analyze(image: ImageProxy) {
        val startTime = System.currentTimeMillis()
        val bitmap = BitmapUtils.getBitmap(image, targetRotation)
        val bitmapTime = System.currentTimeMillis()

        if (bitmap == null) {
            // todo propagate Error (through onSignCallback?)
            Log.e(TAG, "Resulting Bitmap was null")
            image.close()
            return
        }

        val body = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("img", "filename.jpg",
                bitmap.toByteArray().toRequestBody("image/*jpg".toMediaType())).build()

        val request = Request.Builder()
            .url("$connectionURL/api/process")
            .post(body)
            .build()

        val response: Response
        try {
            response = client.newCall(request).execute()
        } catch (e: Exception) {
            onExceptionCallback.accept(e)
            image.close()
            return
        }

        Log.d(TAG, "New Response: $response")

        val responseBody = response.body

        if (responseBody == null) {
            Log.e(TAG, "Response body was null")
            // todo propagate Error (through onSignCallback?)
            image.close()
            return
        }
        val requestTime = System.currentTimeMillis()
        try {
            val bodyText = responseBody.string()
            onSignCallback.accept(bodyText)
        } catch (e: ProtocolException) {
            // On Emulators, OkHttp sometimes throws the following exception.
            // "java.net.ProtocolException: unexpected end of stream"
            // We will just ignore that request (it happens rather seldom, so it should not be
            // noticeable in the app - about once ever 20-30 requests)
            Log.w(TAG, "Conversion of Body to String failed. Dropping the Request!")
        }


        response.close()
        image.close()
        val endTime = System.currentTimeMillis()
        val totalTimeDelta = endTime - startTime
        val bitmapTimeDelta = bitmapTime - startTime
        val requestTimeDelta = requestTime - bitmapTime
        val responseHandlingDelta = endTime - requestTime
        Log.i(TAG, "Prediction took ${totalTimeDelta}ms " +
                "(Bitmap: ${bitmapTimeDelta}ms, " +
                "Request: ${requestTimeDelta}ms, " +
                "ResponseHandling: ${responseHandlingDelta}ms)")

    }

    fun Bitmap.toByteArray(): ByteArray {
        val stream = ByteArrayOutputStream()
        this.compress(Bitmap.CompressFormat.PNG, 90, stream)
        return stream.toByteArray()
    }
}