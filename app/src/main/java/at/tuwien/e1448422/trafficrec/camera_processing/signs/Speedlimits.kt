package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context
import at.tuwien.e1448422.trafficrec.R

abstract class Speedlimit(context: Context) : Sign(context) {
    override fun isNowInvalid(newSignTypes: List<String>): Boolean {
        for (newType in newSignTypes) {
            if (newType.startsWith("speedlimit", true))
                return true
        }
        return false
    }

    override fun getImportance(): Int {
        return Int.MAX_VALUE
    }
}

class Speedlimit20(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_20
}

class Speedlimit30(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_30
}

class Speedlimit50(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_50
}

class Speedlimit60(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_60
}

class Speedlimit70(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_70
}

class Speedlimit80(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_80
}

class Speedlimit100(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_100
}

class Speedlimit120(context: Context): Speedlimit(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_speed_120
}