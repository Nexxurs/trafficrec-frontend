package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context
import at.tuwien.e1448422.trafficrec.R

abstract class WarningSign(context: Context, timeout: Int = DEFAULT_TIMEOUT) : TimeoutSign(context, timeout) {
}

class SharpLeft(val type: String, context: Context) : WarningSign(context, DEFAULT_TIMEOUT_SHORT) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_sharp_left
    override fun type(): String {
        return type
    }
}

class SharpRight(val type: String, context: Context) : WarningSign(context, DEFAULT_TIMEOUT_SHORT) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_sharp_right
    override fun type(): String {
        return type
    }
}

class DoubleCurveLeft(val type: String, context: Context) : WarningSign(context, DEFAULT_TIMEOUT_SHORT) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_double_curve_left
    override fun type(): String {
        return type
    }
}

class RoughStreet(val type: String, context: Context) : WarningSign(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_rough_street
    override fun type(): String {
        return type
    }
}

class SlipperyStreet(val type: String, context: Context) : WarningSign(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_slippery_street
    override fun type(): String {
        return type
    }
}

class StreetNarrows(val type: String, context: Context) : WarningSign(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_street_narrows
    override fun type(): String {
        return type
    }
}

class ConstructionSite(val type: String, context: Context) : WarningSign(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_construction_site
    override fun type(): String {
        return type
    }
}

class TrafficLight(val type: String, context: Context) : WarningSign(context, DEFAULT_TIMEOUT_SHORT) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_traffic_light
    override fun type(): String {
        return type
    }
}

class PedestrianCrossing(val type: String, context: Context) : WarningSign(context, DEFAULT_TIMEOUT_SHORT) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_pedestrian_crossing
    override fun type(): String {
        return type
    }
}

class PlayingChildren(val type: String, context: Context) : WarningSign(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_playing_children
    override fun type(): String {
        return type
    }
}

class ByciclesCrossing(val type: String, context: Context) : WarningSign(context, DEFAULT_TIMEOUT_SHORT) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_bicycles_crossing
    override fun type(): String {
        return type
    }
}

class Snow(val type: String, context: Context) : WarningSign(context, DEFAULT_TIMEOUT_LONG) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_snow
    override fun type(): String {
        return type
    }
}

class DeerCrossing(val type: String, context: Context) : WarningSign(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_deer_crossing
    override fun type(): String {
        return type
    }
}