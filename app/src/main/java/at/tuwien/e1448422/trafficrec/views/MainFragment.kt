package at.tuwien.e1448422.trafficrec.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import at.tuwien.e1448422.trafficrec.R
import at.tuwien.e1448422.trafficrec.databinding.FragmentMainBinding


class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val binding = FragmentMainBinding.inflate(inflater, container, false)

        binding.startBtn.setOnClickListener {
            val sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this.requireContext())
            if (sharedPreferences.getBoolean(getString(R.string.connection_test_success), false))
                this.findNavController().navigate(R.id.action_mainFragment_to_cameraFragment)
            else {
                Toast.makeText(
                    this.requireContext(),
                    "Please check the Connection URL first, before starting this application!",
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        }
        binding.settingsBtn.setOnClickListener {
            this.findNavController().navigate(R.id.action_mainFragment_to_settingsFragment)
        }

        return binding.root
    }


}