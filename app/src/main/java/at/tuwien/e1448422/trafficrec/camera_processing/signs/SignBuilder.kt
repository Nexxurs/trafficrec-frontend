package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context
import java.util.*

class SignBuilder(val context: Context) {
    fun fromTypes(types: List<String>) : List<Sign> {
        val result = LinkedList<Sign>()

        for (type in types) {
            val newSign = fromType(type)
            if (newSign != null) {
                result.add(newSign)
            }
        }

        return result
    }

    fun fromType(type: String): Sign? {
        return when(type) {
            "SPEEDLIMIT_20" -> Speedlimit20(context)
            "SPEEDLIMIT_30" -> Speedlimit30(context)
            "SPEEDLIMIT_50" -> Speedlimit50(context)
            "SPEEDLIMIT_60" -> Speedlimit60(context)
            "SPEEDLIMIT_70" -> Speedlimit70(context)
            "SPEEDLIMIT_80" -> Speedlimit80(context)
            "SPEEDLIMIT_100" -> Speedlimit100(context)
            "SPEEDLIMIT_120" -> Speedlimit120(context)

            "OVERTAKING_PROHIBITED" -> CarOvertakingProhibited(type, context)
            "OVERTAKING_TRUCK_PROHIBITED" -> TruckOvertakingProhibited(type, context)

            "PRIORITY_CROSSING" -> PriorityCrossing(type, context)
            "PRIORITY_STREET" -> PriorityStreet(type, context)
            "GIVE_PRIORITY" -> GivePriority(type, context)
            "STOP" -> Stop(type, context)

            "SHARP_LEFT" -> SharpLeft(type, context)
            "SHARP_RIGHT" -> SharpRight(type, context)
            "DOUBLE_CURVE_LEFT" -> DoubleCurveLeft(type, context)
            "ROUGH_STREET" -> RoughStreet(type, context)
            "SLIPPERY_STREET" -> SlipperyStreet(type, context)
            "STREET_NARROWS" -> StreetNarrows(type, context)
            "CONSTRUCTION_SITE" -> ConstructionSite(type, context)
            "TRAFFIC_LIGHT" -> TrafficLight(type, context)
            "PEDESTRIAN_CROSSING" -> PedestrianCrossing(type, context)
            "PLAYING_CHILDREN" -> PlayingChildren(type, context)
            "BYCICLES_CROSSING" -> ByciclesCrossing(type, context)

            "SNOW" -> Snow(type, context)
            "DEER_CROSSING" -> DeerCrossing(type, context)

            else -> null
        }
    }
}