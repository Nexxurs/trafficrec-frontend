package at.tuwien.e1448422.trafficrec.camera_processing

class Prediction(val type: String, val score: Int) {
    override fun toString(): String {
        return "$type ($score)"
    }
}