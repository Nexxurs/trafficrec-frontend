package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context
import at.tuwien.e1448422.trafficrec.R

class PriorityStreet(val type: String, context: Context): TimeoutSign(context, DEFAULT_TIMEOUT_LONG) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_priority_street
    override fun type(): String {
        return type
    }
}