package at.tuwien.e1448422.trafficrec.camera_processing

import android.content.Context
import android.media.Image
import android.util.Log
import android.util.Pair
import android.view.ViewGroup
import android.widget.GridLayout
import android.widget.GridView
import android.widget.ImageView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.compose.ui.text.android.LayoutCompat
import androidx.preference.PreferenceManager
import at.tuwien.e1448422.trafficrec.R
import at.tuwien.e1448422.trafficrec.camera_processing.signs.Sign
import at.tuwien.e1448422.trafficrec.camera_processing.signs.SignBuilder
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashSet
import kotlin.math.floor

class SignState(val context: Context, val viewGroup: GridLayout) {
    val TAG = "SignState"
    val iconSize = 200

    private val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val signBuilder = SignBuilder(context)
    private val key = context.getString(R.string.prediction_min_score)

    private val currentSigns = HashSet<Sign>()

    fun handleResponse(responseText: String): Set<Sign> {
        val responseJSON = JSONObject(responseText)
        val predictions = toSignPrediction(responseJSON)

        handlePredictions(predictions)

        return currentSigns
    }

    fun renderSigns() {
        // todo sort by importance of the sign (longest lasting first)
        // todo this is ugly af
        val numColumns = viewGroup.width / iconSize
        Log.i(TAG, "Setting Column Count to $numColumns")
        viewGroup.columnCount = numColumns

        viewGroup.removeAllViews()

        for (sign in currentSigns.sortedBy { it.getImportance()*-1 }) {
            try {
                val imgView = ImageView(context)
                imgView.layoutParams = ViewGroup.LayoutParams(iconSize, iconSize)
                imgView.setImageBitmap(sign.getBitmap())
                viewGroup.addView(imgView)
            }catch (_: IllegalStateException) {
                Log.e(TAG, "Sign $sign does not have a valid Bitmap!")
            }
        }
    }

    private fun toSignPrediction(json: JSONObject): List<Prediction> {
        val minScore = Integer.parseInt(sharedPreferences.getString(key, "50")!!)

        val predictions = json.getJSONArray("predictions")
        val result = LinkedList<Prediction>()
        for (i in 0 until predictions.length()) {
            val current = predictions.getJSONObject(i)
            val type = current.getString("class")
            val score = current.getInt("score")

            if(score < minScore)
                continue

            val prediction = Prediction(type, score)
            Log.i("PREDICTION", prediction.toString())
            result.add(prediction)
        }
        return result
    }

    private fun handlePredictions(predictions: List<Prediction>) {
        // todo if multiple new values are invalid together
        //  we should only use the one with the best prediction score
        val newTypes = predictions.map { it.type.uppercase() }
        currentSigns.removeIf { it.isNowInvalid(newTypes) }

        // Check if any of the new Signs are cancelling each other
        // -> only use the ones with the best scores
        val newSignPairs = newTypes.map { Pair(it, signBuilder.fromType(it)) }
        val acceptedSignTypes = LinkedList<String>()

        for (signPair in newSignPairs) {
            if (signPair.second == null)
                continue

            if (signPair.second!!.isNowInvalid(acceptedSignTypes))
                continue

            currentSigns.add(signPair.second!!)
            acceptedSignTypes.add(signPair.first)
        }
    }
}