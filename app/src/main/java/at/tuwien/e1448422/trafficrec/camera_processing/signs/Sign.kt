package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import at.tuwien.e1448422.trafficrec.R


abstract class Sign(val context: Context) {
    abstract fun isNowInvalid(newSignTypes: List<String>): Boolean

    abstract fun getDrawableResId(): Int
    abstract fun getImportance(): Int

    open fun getBitmap(): Bitmap {
        val drawable = ResourcesCompat.getDrawable(context.resources, getDrawableResId(), null)
            ?: throw IllegalStateException()

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

    override fun toString(): String {
        return this.javaClass.simpleName
    }
}