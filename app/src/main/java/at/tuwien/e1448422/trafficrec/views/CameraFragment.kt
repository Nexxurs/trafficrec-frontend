package at.tuwien.e1448422.trafficrec.views

import android.Manifest
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.Size
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleOwner
import at.tuwien.e1448422.trafficrec.camera_processing.SignAnalyzer
import at.tuwien.e1448422.trafficrec.camera_processing.SignState
import at.tuwien.e1448422.trafficrec.databinding.FragmentCameraBinding
import at.tuwien.e1448422.trafficrec.utils.SuccessFailureCallback
import kotlinx.coroutines.Runnable
import java.lang.Exception
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class CameraFragment : Fragment() {

    val TAG = "CameraFragment"

    lateinit var binding: FragmentCameraBinding
    lateinit var cameraExecutor: ExecutorService
    lateinit var signState: SignState
    lateinit var signAnalyzer: SignAnalyzer


    private val orientationEventListener by lazy {
        object : OrientationEventListener(this.requireContext()) {
            override fun onOrientationChanged(orientation: Int) {
                if (orientation == ORIENTATION_UNKNOWN) {
                    return
                }

                val rotation = when (orientation) {
                    in 45 until 135 -> 180
                    in 135 until 225 -> 270
                    in 225 until 315 -> 0
                    else -> 90
                }
                signAnalyzer.targetRotation = rotation
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCameraBinding.inflate(inflater, container, false)
        binding.infoText.text = "Nothing yet... "

        cameraExecutor = Executors.newSingleThreadExecutor()
        signState = SignState(requireContext(), binding.signWrapper)

        binding.cameraShowHideBtn.setOnClickListener {
            if (binding.cameraPreview.isVisible) {
                binding.cameraPreview.visibility = View.GONE
            } else {
                binding.cameraPreview.visibility = View.VISIBLE
            }
        }

        checkPermission(Manifest.permission.CAMERA, object : SuccessFailureCallback {
            override fun onSuccess() {
                println("Permission granted")
                startCamera()
            }

            override fun onFailure(errorMessage: String) {
                println("Permission denied")
                binding.infoText.text = "Error: $errorMessage\nPlease grant permission!"
            }
        })

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        orientationEventListener.disable()
        cameraExecutor.shutdown()
    }


    fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this.requireContext())
        cameraProviderFuture.addListener(
            {
                val cameraProvider = cameraProviderFuture.get()

                val preview = Preview.Builder().build()
                preview.setSurfaceProvider(binding.cameraPreview.surfaceProvider)

                val cameraSelector = CameraSelector.Builder()
                    .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                    .build()

                val imageAnalysis = ImageAnalysis.Builder()
                    .setOutputImageFormat(ImageAnalysis.OUTPUT_IMAGE_FORMAT_YUV_420_888)
                    .setTargetResolution(Size(800, 500))
                    .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                    .build()

                signAnalyzer = SignAnalyzer(requireContext(),
                    onSignCallback@{
                        Log.d("New Sign Response", it)
                        var status: String
                        try {
                            val predictions = signState.handleResponse(it)
                            status = predictions.joinToString("\n")

                        } catch (e: Exception) {
                            Log.e(TAG, "Cannot handle Server response")
                            status = "Cannot handle Server Response\n$it"
                            e.printStackTrace()
                        }

                        // This is used instead of requireActivity, because there might be a race
                        // condition when the activity gets closed while a request is parsed.
                        val activity = this.activity ?: return@onSignCallback
                        activity.runOnUiThread {
                            signState.renderSigns()
                            binding.infoText.text = status
                        }
                    },
                    onExceptionCallback@{
                        val activity = this.activity ?: return@onExceptionCallback
                        activity.runOnUiThread {
                            binding.infoText.text = it.toString()
                        }
                    })
                imageAnalysis.setAnalyzer(cameraExecutor, signAnalyzer)

                // This needs to happen after SignAnalyzer is created
                orientationEventListener.enable()

                var camera = cameraProvider.bindToLifecycle(
                    this as LifecycleOwner,
                    cameraSelector,
                    preview,
                    imageAnalysis
                )

            }, ContextCompat.getMainExecutor(this.requireContext())
        )
    }

    private fun checkPermission(permission: String, callback: SuccessFailureCallback) {
        val requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission())
            { isGranted: Boolean ->
                if (isGranted) callback.onSuccess()
                else callback.onFailure("Permission not granted")
            }

        when {
            ContextCompat.checkSelfPermission(
                this.requireContext(),
                permission
            ) == PackageManager.PERMISSION_GRANTED -> {
                callback.onSuccess()
            }
            shouldShowRequestPermissionRationale(permission) -> {
                callback.onFailure("Permission not granted")
            }
            else -> {
                // asking for permission..
                requestPermissionLauncher.launch(permission)
            }
        }
    }
}