package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context
import at.tuwien.e1448422.trafficrec.R

abstract class OvertakingProhibited(context: Context) : TimeoutSign(context, DEFAULT_TIMEOUT_LONG) {
    override fun isNowInvalid(newSignTypes: List<String>): Boolean {
        if(super.isNowInvalid(newSignTypes))
            return true
        val cancelTypeName = "${type()}_CANCEL"
        return newSignTypes.contains(cancelTypeName)
    }
}

class CarOvertakingProhibited(val type: String, context: Context): OvertakingProhibited(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_car_overtaking_prohibited
    override fun type(): String {
        return type
    }
}

class TruckOvertakingProhibited(val type: String, context: Context): OvertakingProhibited(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_truck_overtaking_prohibited
    override fun type(): String {
        return type
    }
}