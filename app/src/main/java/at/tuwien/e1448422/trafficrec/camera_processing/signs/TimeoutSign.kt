package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context

abstract class TimeoutSign(context: Context, val validForSeconds: Int) : Sign(context) {
    companion object {
        val DEFAULT_TIMEOUT_LONG = 2*60
        val DEFAULT_TIMEOUT = 30
        val DEFAULT_TIMEOUT_SHORT = 5
    }

    private var endTime : Long = 0
    init {
        resetTimer()
    }

    private fun resetTimer() {
       endTime = System.currentTimeMillis() + (validForSeconds * 1000)
    }

    override fun getImportance(): Int {
        return validForSeconds
    }

    override fun isNowInvalid(newSignTypes: List<String>): Boolean {
        if (newSignTypes.contains(type())) {
            resetTimer()
        }
        if (System.currentTimeMillis() > endTime)
            return true
        return false
    }

    /**
     * has to be the correct type (in uppercase letters)
     */
    abstract fun type(): String
}