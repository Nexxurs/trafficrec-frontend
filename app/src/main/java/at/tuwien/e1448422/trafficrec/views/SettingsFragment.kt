package at.tuwien.e1448422.trafficrec.views

import android.os.Bundle
import android.os.Handler
import android.text.InputType
import android.util.Log
import android.widget.Toast
import androidx.core.content.edit
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import at.tuwien.e1448422.trafficrec.R
import at.tuwien.e1448422.trafficrec.utils.SuccessFailureCallback
import okhttp3.*
import java.io.IOException
import java.lang.IllegalArgumentException

class SettingsFragment : PreferenceFragmentCompat() {

    private val TAG = "Settings Fragment"
    private val client = OkHttpClient()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)

        val sharedPreferences = preferenceManager.sharedPreferences

        val connectionTester = findPreference<Preference>(getString(R.string.connection_test_btn))
            ?: throw RuntimeException("No Connection Test Button found")
        connectionTester.setOnPreferenceClickListener {
            Log.i(TAG, "Connection Tester: Button clicked")

            connectionTester.setIcon(R.drawable.ic_unknown)

            val currUrl = sharedPreferences.getString(getString(R.string.connection_url), "")
                ?: throw RuntimeException("Shared Preferences returns null string (even with default value) ?!")

            checkUrl(currUrl, object : SuccessFailureCallback {
                override fun onSuccess() {
                    sharedPreferences.edit {
                        putBoolean(getString(R.string.connection_test_success), true)
                    }
                    connectionTester.setIcon(R.drawable.ic_success)
                }

                override fun onFailure(errorMessage: String) {
                    Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
                    sharedPreferences.edit {
                        putBoolean(getString(R.string.connection_test_success), false)
                    }
                    connectionTester.setIcon(R.drawable.ic_failure)
                }
            })

            true
        }
        if (sharedPreferences.getBoolean(getString(R.string.connection_test_success), false))
            connectionTester.setIcon(R.drawable.ic_success)

        val urlPreference = findPreference<EditTextPreference>(getString(R.string.connection_url))
            ?: throw RuntimeException("No URL Preference found")
        urlPreference.setOnPreferenceChangeListener { _, _ ->
            connectionTester.setIcon(R.drawable.ic_unknown)
            sharedPreferences.edit {
                putBoolean(getString(R.string.connection_test_success), false)
            }
            true
        }

        val predictionMinScore = findPreference<EditTextPreference>(getString(R.string.prediction_min_score))
        predictionMinScore!!.setOnBindEditTextListener {
            it.inputType = InputType.TYPE_CLASS_NUMBER
        }
    }

    private fun checkUrl(url: String, callback: SuccessFailureCallback) {
        val requestUrl = "$url/version"

        Log.i(TAG, "Checking Url $requestUrl")

        val request: Request
        try {
            request = Request.Builder()
                .url(requestUrl)
                .build()
        } catch (e: IllegalArgumentException) {
            callback.onFailure(e.message ?: "Invalid URL")
            return
        }

        val mainHandler = Handler(this.requireContext().mainLooper)

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
                mainHandler.post {
                    callback.onFailure(e.message ?: "Unable to connect to Server")
                }
            }

            override fun onResponse(call: Call, response: Response) {
                if (!response.isSuccessful) {
                    Log.e(TAG, "Failure: $response")
                    mainHandler.post {
                        callback.onFailure("Bad status code ${response.code}")
                    }
                } else {
                    val resBody = response.body!!.string()
                    Log.i(TAG, "Success: $resBody")
                    if (resBody.startsWith("BA V"))
                        mainHandler.post {
                            callback.onSuccess()
                        }
                    else {
                        Log.e(TAG, "Version Endpoint didnt respond correctly!")
                        mainHandler.post {
                            callback.onFailure("Wrong response on Version endpoint")
                        }
                    }
                }
            }
        })
    }
}