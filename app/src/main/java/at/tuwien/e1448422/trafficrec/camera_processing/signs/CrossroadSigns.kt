package at.tuwien.e1448422.trafficrec.camera_processing.signs

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat
import at.tuwien.e1448422.trafficrec.R

abstract class CrossroadSign(context: Context) : TimeoutSign(context, DEFAULT_TIMEOUT_SHORT) {
}

class PriorityCrossing(val type: String, context: Context) : CrossroadSign(context) {
    override fun type(): String {
        return type
    }
    override fun getDrawableResId(): Int = R.drawable.ic_sign_priority_crossing
}

class GivePriority(val type: String, context: Context) : CrossroadSign(context) {
    override fun type(): String {
        return type
    }
    override fun getDrawableResId(): Int = R.drawable.ic_sign_give_priority
}

class Stop(val type: String, context: Context) : CrossroadSign(context) {
    override fun getDrawableResId(): Int = R.drawable.ic_sign_stop

    override fun type(): String {
        return type
    }
}